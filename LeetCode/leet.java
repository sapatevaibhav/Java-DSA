// Given an integer x, return true if x is a palindrome, and false otherwise.

public class leet {
    public static void main(String[] args) {
        System.out.println(isPalindrome(12221));
    }

    public static boolean isPalindrome(int x) {
        int sum = 0, c = x;
        while (x > 0) {
            int rem = x % 10;
            sum = rem + (sum * 10);
            x = x / 10;
        }
        if (c == sum) {
            return true;
        } else {
            return false;
        }
    }
}
