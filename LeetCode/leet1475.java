// You are given an integer array prices where prices[i] is the price of the ith item in a shop.

// There is a special discount for items in the shop. If you buy the ith item, then you will receive a discount equivalent to prices[j] where j is the minimum index such that j > i and prices[j] <= prices[i]. Otherwise, you will not receive any discount at all.

// Return an integer array answer where answer[i] is the final price you will pay for the ith item of the shop, considering the special discount.

import java.util.Arrays;

/**
 * leet1475
 */
public class leet1475 {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(finalPrices(new int[] { 10, 1, 1, 6 })));
    }

    public static int[] finalPrices(int[] prices) {
        // int[] arr = new int[prices.length];
        for (int i = 0; i < prices.length - 1; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                if (prices[j] <= prices[i]) {
                    prices[i] = prices[i] - prices[j];
                    break;
                }

            }
        }
        return prices;
    }
}